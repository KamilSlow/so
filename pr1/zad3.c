#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <wait.h>
int main()
{
printf("Proces macierzysty:\n");
printf("UID: %d\t GID: %d\t PID: %d\t PPID: %d\n\n", getuid(), getgid(), getpid(), getppid());
int x=0;
printf("Procesy potomne:\n");
for(int i=1; i<4; i++)
{
	switch (fork())
	{
		case -1:
			perror("fork error");
			exit (1);
		case 0:
			if(execl("./zad1", "zad1",NULL)==-1)
			{
				perror("execl error");
				exit  (1);
			}
			break;
		default:
			//wait(&x);
			break;	
	}
}
int w=0;
for(int i=1; i<4; i++)
{
w=wait(&x);
if(w==-1)
{
perror("wait error");
}
else
printf(" PID: %d\t status: %d\n",w, x);
}
exit (0);
}
