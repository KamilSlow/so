#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <wait.h>
int main()
{

int x=0;
int pid = getpid();
char komenda[20];
sprintf(komenda, "pstree -p %d", pid);
for(int i=0; i<3; i++)
{		
	if(fork()==-1)
	{
		perror("fork error");
		exit(1);
	}
}
if(getpid()==pid)
{
	system(komenda);
}
sleep(1);
wait(&x);
printf("UID: %d\t GID: %d\t PID: %d\t PPID: %d\n", getuid(), getgid(), getpid(), getppid());
exit (0);
}
