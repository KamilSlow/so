#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
int tab[2][10];
void * watek1()
{
int suma1=0;
pthread_t ttit=0;
ttit=pthread_self();
printf("\nTid pierwszego watku: %lu\n",ttit);
for(int i=0; i<10; i++)
	suma1+=tab[0][i];
//pthread_exit((void*)suma1);
}
void * watek2()
{
int suma2=0;
pthread_t ttit=0;
ttit=pthread_self();
printf("\nTid drugiego watku: %lu\n",ttit);
for(int i=0; i<10; i++)
	suma2+=tab[1][i];
pthread_exit((void*)suma2);
}
int main()
{
srand(time(NULL));
int i, j,suma=0, s1=0, s2=0;
pthread_t tid1,tid2;
for(i=0; i<2; i++)
{
    for(j=0; j<10; j++)
    {
    tab[i][j]=rand()%10;
    }
}
for(i=0; i<2; i++)
{
    for(j=0; j<10; j++)
    {
    printf("%d\t",tab[i][j]);
    }
    printf("\n");
}
printf("\n");
if  (pthread_create(&tid1,NULL,watek1,NULL)==-1)
    {
     printf("Nie udalo sie utworzyc watku 1");
     exit(1);
    }
if  (pthread_create(&tid2,NULL,watek2,NULL)==-1)
    {
     printf("Nie udalo sie utworzyc watku 2");
     exit(1);
    }
if(pthread_join(tid1,(void**)&s1)==-1)
    {
     printf("Nie udalo sie przylaczyc watku 1");
     exit(2);
    }

if (pthread_join(tid2,(void**)&s2)==-1)
    {
     printf("Nie udalo sie przylaczyc watku 2");
     exit(2);
    }
suma=s1+s2;
    printf("Suma wyliczona przez pierwszy watek: %d\n", s1);
    printf("Suma wyliczona przez drugi watek: %d\n", s2);
    printf("Suma wartosci zwroconych przez oba watki: %d\n",suma);
exit(0);
}
