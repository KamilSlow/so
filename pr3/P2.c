#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>


int semafor;
void semafor_p(int num);
void semafor_v(int num);
void task(int i, int j);


int main(int argc,int * argv[])
  {
semafor=semget(12,3,0600|IPC_CREAT);
	task(2,1);
	semafor_v(1);
	semafor_p(2);
	task(2,2);
	semafor_v(0);
	semafor_p(2);
	task(2,3);
	semafor_v(1);
    exit(0);
  }

void semafor_p(int num)
  {
    int zmien_sem;
    struct sembuf bufor_sem;
    bufor_sem.sem_num=num;
    bufor_sem.sem_op=-1;
    bufor_sem.sem_flg=SEM_UNDO;
    zmien_sem=semop(semafor,&bufor_sem,1);
    if (zmien_sem==-1) 
      {
        printf("Nie moglem zamknac semafora.\n");
        exit(1);
      }
  }

void semafor_v(int num)
  {
    int zmien_sem;
    struct sembuf bufor_sem;
    bufor_sem.sem_num=num;
    bufor_sem.sem_op=1;
    bufor_sem.sem_flg=SEM_UNDO;
    zmien_sem=semop(semafor,&bufor_sem,1);
    if (zmien_sem==-1) 
      {
        printf("Nie moglem otworzyc semafora.\n");
        exit(2);
      }
  }
void task(int i, int j)
{
    	FILE *fp;
	if ((fp=fopen("test.txt", "a+")),fp==NULL) 
	{
     		printf ("Nie moge otworzyc pliku\n");
     		exit(3);
    	}
	printf("Sekcja t%d%d procesu o PID= %d\n",i,j,getpid());
	fprintf(fp,"Sekcja t%d%d procesu o PID= %d\n",i,j,getpid());
    	fclose (fp);
	sleep(1);
}
