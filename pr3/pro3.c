#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <wait.h>
#include <errno.h>
#include <string.h>

int semafor;
char temp[2][20];

void nowy_semafor();
void ustaw_semafor(); 
void procesy(); 
void czekaj();
void usun_semafor();

int main()
{
    nowy_semafor();
    ustaw_semafor();
    procesy();
    czekaj();
    usun_semafor();
    return 0;
}

void czekaj()
{
int x=0;
for(int i=0; i<3; i++)
{
    if(wait(&x)==-1)
    {
    perror("wait error");
    }
}
}

void nowy_semafor()
{
semafor=semget(12,3,0600|IPC_CREAT);
    if (semafor==-1) 
      {
        printf("Nie moglem utworzyc nowego semafora\n %s\n", strerror(errno));
        exit(1);
      }
}
void ustaw_semafor()
  {
    int ustaw_sem;
for(int i;i<3;i++)
{
    ustaw_sem=semctl(semafor,i,SETVAL,0);
    if (ustaw_sem==-1)
      {
        printf("Nie mozna ustawic semafora numer %d\n %s\n", i, strerror(errno));
        exit(2);
      }
}
}
void usun_semafor()  
  {
    int sem;
    sem=semctl(semafor,3,IPC_RMID);
    if (sem==-1)
      {
        printf("Nie mozna usunac semafora\n %s\n",strerror(errno));
        exit(3);
      }
  }
void procesy()
{
for(int i=1; i<4; i++)
{
	switch (fork())
	{
		case -1:
			perror("fork error");
			exit (1);
		case 0:
                sprintf(temp[0],"./P%d",i);
                sprintf(temp[1],"P%d",i);
			    if(execl(temp[0],temp[1],NULL)==-1)
			    {
				    perror("execl error");
				    exit  (1);
                }
			break;
		default:
			break;	
	}
}
}
